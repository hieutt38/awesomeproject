import GLOBAL from '../utils/Globals'
const NewsApi = { 
    fetch(category, page){
        var url = GLOBAL.BASE_URL + GLOBAL.API.FETCH_NEWS + category +'?page='+page;
        return fetch(url,{
            method: 'GET',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => res.json())
    },
    getDetail(id){
        var url = GLOBAL.BASE_URL + GLOBAL.API.GET_DETAIL + id;
        return fetch(url,{
            method: 'GET',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(res => res.json())
    }
};

module.exports = NewsApi;