import React from 'react';
import { StackNavigator } from 'react-navigation';
import Login from './components/auth/Login';
import Tab from './TabRoute';
import NewsDetail from './components/news/NewsDetail';
export const Navigator = StackNavigator(
    {
        Login:{
            screen:Login
        },
        Tab : {
            screen: Tab
        },
        // TabWrapper:{
        //     screen:TabWrapper
        // },
        NewsDetail:{
            path: 'newsDetail/:id',
            screen:NewsDetail
        }
    },
    { 
        // headerMode: 'none',
        portraitOnlyMode: true
    }
)