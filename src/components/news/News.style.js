import { StyleSheet, Dimensions, Platform} from 'react-native';
const {height, width} = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    item: {
        width:width - 5,
        flex: 0
    },
    detailContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    textJustify:{
        textAlign:'justify',
        fontSize: 14
    },
    tab:{
        marginTop: Platform.OS == 'ios' ? 20 : 0
    },
    image : {
        height: 200, width: width-40, flex: 1, resizeMode:'stretch'
    },
    desText:{
        textAlign:'justify',
        fontSize: 13,
        color: 'gray',
        marginTop: 10
    },
    footer: {
        paddingVertical : 15,
    },
    detailContainer:{
        flexDirection : 'row',
        flex: 1,
        justifyContent: 'center',
    },
    detailImage:{
        height: 200, width: width, resizeMode:'stretch'
    },
    title:{
        // textAlign:'justify',
        fontSize: 14,
        color: '#F5FCFF',
        backgroundColor: '#1976d2',
        padding: 10
    },
    htmlContent:{
        marginTop: 35
    },
    p : {
        textAlign:'justify',
        paddingLeft: 10,
        paddingRight: 10,
        // margin: 0,
        marginTop: 0,
        marginBottom: 0,
        paddingTop: 0,
        paddingBottom: 0
    },
    img: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    table : {
        borderWidth : 1,
        borderColor: 'gray'
    }
});