import React, { Component } from 'react';
import {View,FlatList, ActivityIndicator, Text} from 'react-native';

import styles from './News.style';
import NewsApi from '../../apis/NewsApi';
import FlatListRow from './FLatListRow';
import Spinner from 'react-native-spinkit';

export default class News extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded:false,
            page:1,
            news : [],
            loading: false
        };
    }  

    componentWillMount(){
        this.fetchData();
    }

    fetchData(){
        if(!this.state.loading){
            this.setState({loading: true});
            NewsApi.fetch(6,this.state.page)
            .then(res => {
                // console.log(res.news);
                this.setState({loading: false});
                if(res.status !== undefined && res.status == 1){
                    this.setState({
                        news : [...this.state.news, ...res.news],
                        loaded: true,
                        page : this.state.page + 1,
                        refreshing: false
                        });
                }else{
                    
                }
            })
            .catch(err => {
                this.setState({loading: false});
            })
            .done();
        }
    }

    handleRefresh(){
        this.setState({page: 1, refreshing: true})
        this.fetchData();
    }

    loadMore(){
        if(this.state.page > 1)
            this.fetchData();
    }

    renderFooter(){
        if(!this.state.loading) return null;
        return(
            <View style={styles.footer}>
                <ActivityIndicator animating size='large' />
            </View>
        );
    }

    renderFlatlistRow(item){
        return (
            <FlatListRow navigation={this.props.navigation} item={item}/>
        );
    }

    render() {
        return (
        <View style={styles.container}>
            {
                this.state.loaded
                &&
                <FlatList
                    data={this.state.news}
                    renderItem={({item}) => 
                    this.renderFlatlistRow(item)
                    }
                    keyExtractor={item => item.id}
                    removeClippedSubviews={true}
                    shouldItemUpdate={(props,nextProps)=>{ 
                                        return props.item!==nextProps.item}}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh.bind(this)}
                    onEndReached={this.loadMore.bind(this)}
                    onEndReachedThreshold={1}
                    initialNumToRender={20}
                    maxToRenderPerBatch={20}
                    windowSize={20}
                />
                ||
                <Spinner 
                    //style={styles.spinner} 
                    isVisible={true}
                    size={80} 
                    type='ThreeBounce'
                    color='#1976d2'/>
            }
        </View>
        );
    }
}