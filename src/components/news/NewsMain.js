import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, Animated , Platform} from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import News from './News';
import Promotion from './Promotion';
import Loyalty from './Loyalty';
import Business from './Business';
import { Icon} from 'native-base';

import type { NavigationState } from 'react-native-tab-view/types';

type Route = {
  key: string,
  title: string,
};

type State = NavigationState<Route>;

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

export default class TopBarTextExample extends PureComponent<*, State> {
//   static title = 'Scrollable top bar';
  static appbarElevation = 0;

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
        tabBarLabel: 'Tin tức',
        header: null,
        tabBarIcon: ({ tintColor }) => (
            <Icon style={{color: tintColor}} name="paper" />
        )
    };
};


  state = {
    index: 0,
    routes: [
      { key: '1', title: 'Tin tức chung' },
      { key: '2', title: 'Khuyến mãi' },
      { key: '3', title: 'Kết nối dài lâu' },
      { key: '4', title: 'Khách hàng doanh nghiệp' },
    ],
  };

  _handleIndexChange = index => {
    this.setState({
      index,
    });
  };
  _renderIndicator = (position) => {
    return ({ index }) => {
      let style = {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 2
      }

      if (position === index) {
        style = {
          ...style,
          borderTopWidth: 2,
          borderColor: '#ffeb3b'
        }
      }

      return (<Animated.View style={style} />)
    }
  }

  _renderHeader = props => {
    return (
      <TabBar
        {...props}
        renderLabel={this._renderLabel(props)}
        renderIndicator={() => null}  
        /* renderIcon={this._renderIndicator(props.navigationState.index)} */
        scrollEnabled
        style={styles.tabbar}
        tabStyle={styles.tab}
        labelStyle={styles.label}
        pressColor="rgba(255, 64, 129, .5)"
      />
    );
  };

  _renderLabel = props => ({ route, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const outputRange = inputRange.map(
      inputIndex => (inputIndex === index ? '#D6356C' : '#222')
    );
    const color = props.position.interpolate({
      inputRange,
      outputRange,
    });

    return (
      <Animated.Text style={[styles.label, { color }]}>
        {route.title}
      </Animated.Text>
    );
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case '1':
        return (
          <News
            /* state={this.state} */
            navigation={this.props.navigation}
          />
        );
      case '2':
        return (
          <Promotion
            /* state={this.state} */
            navigation={this.props.navigation}
          />
        );
      case '3':
        return (
          <Loyalty
            /* state={this.state} */
            navigation={this.props.navigation}
          />
        );
      case '4':
        return (
          <Business
            /* state={this.state} */
            navigation={this.props.navigation}
          />
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <TabViewAnimated
        style={[styles.container, this.props.style]}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#fff',
  },
  tab: {
    width: null,
    marginTop: Platform.OS == 'ios' ? 20: 0,
    alignItems: 'center',
  },
  indicator: {
    backgroundColor: '#ffeb3b',
  },
  label: {
    fontSize: 14,
    fontWeight: '600',
  },
});