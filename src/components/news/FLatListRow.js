import React, { Component } from 'react';
import {Platform, StyleSheet, View,Alert,FlatList,Image, TouchableHighlight, ActivityIndicator,TouchableOpacity} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import styles from './News.style';
import ic_news from '../../../assets/news.png';
export default class FlatListRow extends Component {
    constructor(props){
        super(props);
    }

    gotoDetail(id){
        this.props.navigation.navigate('NewsDetail',{'id':id});
    }

    render() {
        return (
            <TouchableOpacity onPress={this.gotoDetail.bind(this, this.props.item.id)} underlayColor='white'>
                <Card style={styles.item} >
                    <CardItem>
                        <Left>
                            {<Thumbnail source={ic_news} />}
                            <Body>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={styles.textJustify}>{this.props.item.title}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem>
                        <Body>
                            {
                            this.props.item.image != null
                            &&
                            <Image source={{uri: this.props.item.image}} style={styles.image}/>
                            }
                            <Text numberOfLines={4} ellipsizeMode='tail' style={styles.desText}>{this.props.item.short_description}</Text>
                        </Body>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    }
}