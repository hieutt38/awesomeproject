import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,Image,ScrollView
} from 'react-native';
import {Icon, Button } from "native-base";
import HTMLView from 'react-native-htmlview';
import styles from './News.style';
import NewsApi from '../../apis/NewsApi';
import Spinner from 'react-native-spinkit';

export default class NewsDetail extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            headerStyle: { backgroundColor: 'white', height: 35 },
            headerLeft: <Button transparent primary onPress={() => params.handleBack(navigation)} ><Icon name='md-arrow-back' /></Button>
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            loaded:false,
            newsId: this.props.navigation.state.params.id
        };
    }
    
    

    componentDidMount(){
        this.props.navigation.setParams({ handleBack: this.goBack });
    }

    componentWillMount(){
        NewsApi.getDetail(this.state.newsId)
        .then(res => {
            // console.log(res.news);
            this.setState({loading: false});
            if(res.status !== undefined && res.status == 1){
                this.setState({
                    news : res.news,
                    loaded: true
                    });
            }else{
                
            }
        })
        .catch(err => {
            this.setState({loading: false});
        })
        .done();
    }

    goBack(navigation) {
        navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.loaded
                    &&
                    <ScrollView>
                        {
                            this.state.news.image != null
                            &&
                            <Image source={{uri: this.state.news.image}} style={styles.detailImage}/>
                        }
                        <Text style={styles.title}>{this.state.news.title}</Text>
                        <View style={styles.htmlContent}>
                            <HTMLView
                                value={this.state.news.full_description.replace(/(?:\\[rn]|[\r\n]|[\t\t\r\n]+)+/g, "").replace(/(?:\\[&nbsp]+)+/g, "")}
                                stylesheet={styles}
                            />
                        </View>
                    </ScrollView>
                    ||
                    <Spinner 
                    //style={styles.spinner} 
                        isVisible={true}
                        size={80} 
                        type='ThreeBounce'
                        color='#1976d2'/>
                }
            </View>
        );
    }
}