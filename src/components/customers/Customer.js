import React, { Component } from 'react';
import {
    Text,
    View, StyleSheet
} from 'react-native';
//import styles from '../auth/Login.style';
import { Icon, Header, Body, Title, Content, Container } from 'native-base';
import MapView from 'react-native-maps';
// var MapView = require('react-native-maps');
export default class Login extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            tabBarLabel: 'Bản đồ',
            header: null,
            tabBarIcon: ({ tintColor }) => (
                <Icon style={{color: tintColor}} name="map" />
            )
        };
    };

    constructor(props){
        super(props);
        this.state = {region : this.getInitialState.region};
    }

    getInitialState() {
        return {
          region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
        };
    }
      
    onRegionChange(region) {
        this.setState({ region });
    }

    render() {
        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Native Maps</Title>
                    </Body>
                </Header>
                {/* <Content> */}
                    <MapView
                        style={ styles.map }
                        region={this.state.region}
                        onRegionChange={this.onRegionChange.bind(this)}
                    />
                {/* </Content> */}
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      position: 'absolute',
      top: 65,
      left: 0,
      right: 0,
      bottom: 0,
    },
  });