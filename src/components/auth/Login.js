import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar ,
  TouchableOpacity
} from 'react-native';
// import styles from './Login.style';
import LogoMobifone from './LogoMobiFone';
import Form from './Form';
import Navigator from '../../Route';
export default class Login extends Component<{}> {

  static navigationOptions = {
      header: null
  };

  render() {
    return(
      <View style={stylesLogin.container}>
        <LogoMobifone/>
        <Form type="Login" navigation={this.props.navigation}/>
        <View style={stylesLogin.signupTextCont}>
          <Text style={stylesLogin.signupText}>Don't have an account yet?</Text>
        </View>
      </View>
    );
  }
}

const stylesLogin = StyleSheet.create({
  container : {
    backgroundColor:'#ffffff',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  signupTextCont : {
    flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:50,
    flexDirection:'row'
  },
  signupText: {
    color:'#ffffff',
    fontSize:16
  },
  signupButton: {
    color:'#ffffff',
    fontSize:16,
    fontWeight:'500'
  }
});