import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity 
} from 'react-native';
export default class Form extends Component<{}> {
 
   constructor(props){
      super(props);
  }

  gotoMain(){
      this.props.navigation.navigate('Tab');
    }

  render(){
    return(
      <View style={styles.container}>
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='#000000' 
              placeholder="Username"
              placeholderTextColor = "#9e9e9e"
              keyboardType="email-address"
              onSubmitEditing={()=> this.password.focus()}
              />
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='#000000' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#9e9e9e"
              ref={(input) => this.password = input}
              />  
           <TouchableOpacity style={styles.button} onPress={this.gotoMain.bind(this)} >
             <Text style={styles.buttonText}>{this.props.type}</Text>
           </TouchableOpacity>     
      </View>
      )
  }
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },

  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#000',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#1976d2',
     borderRadius: 10,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
  
});