import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TextInput, TouchableOpacity,KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback
} from 'react-native';
import { Icon, Header, Body, Title, Content, Container } from 'native-base';
import styles from './Home.style';
var MyObjcClass = require('NativeModules').MyObjcClass;

export default class Home extends Component {

    constructor(props){
        super(props);
        this.state = {
            number : 0
        }
    }

    static navigationOptions = {
        tabBarLabel: 'Native',
        header: null,
        tabBarIcon: ({ tintColor }) => (
            <Icon active style={{color: tintColor}}  name="cog" />
        ),
    };
    
    squareMe(num) {
        if (num == '') {
          return;
        }
        num = parseInt(num);
        if(Platform.OS == 'ios'){
            MyObjcClass.squareMe(num, (error, number) => {
            if (error) {
                console.error(error);
            } else {
                this.setState({number: number});
            }
            })
        }
    }

    dismissKeyboard(){
        Keyboard.dismiss
    }

    render() {
        return (
            <Container>
            {/* <View style={styles.container}> */}
                {
                    Platform.OS == 'ios'
                    &&
                    <Header>
                        <Body>
                            <Title>Native Bridge</Title>
                        </Body>
                    </Header>
                }

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        <TextInput style={styles.inputText} onChangeText={(text) => this.squareMe(text)}
                            underlineColorAndroid='transparent' 
                            placeholder="Enter number"
                            placeholderTextColor = "#9e9e9e"
                            keyboardType="numeric"
                            onSubmit={Keyboard.dismiss}
                        />
                        <Text style={styles.text}>
                            {this.state.number}
                        </Text>   
                    </View>
                </TouchableWithoutFeedback> 
            {/* </View> */}
            </Container>
        );
    }
}