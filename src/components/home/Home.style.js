import { StyleSheet, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    inputText: {
        height: 40, 
        borderColor: 'gray', 
        borderBottomWidth: 1,
        width: width - 100
    },
    text : {
        fontSize : 30,
        color: 'red',
        marginTop: 50
    },
    touch: {
        flex : 1,
    },
    touchContent: {
        justifyContent: 'center',
        alignItems: 'center',
    }

});