module.exports = {
    BASE_URL: 'https://mymobi.mobifone.vn/worklight/adapters/MyMobiFoneAdapter/',
    API: {
        FETCH_NEWS : 'getNewsInCategory/',
        GET_DETAIL : 'getNewsDetail/',
    }
}