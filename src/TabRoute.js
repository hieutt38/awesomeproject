import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import Home from './components/home/Home';
import News from './components/news/News';
import Customer from './components/customers/Customer';
import NewsDetail from './components/news/NewsDetail';
import NewsMain from './components/news/NewsMain';
import {Platform} from 'react-native';

const NewsTab = StackNavigator(
    {
        NewsMain:{
            screen:NewsMain
        },
        // NewsDetail:{
        //     path: 'newsDetail/:id',
        //     screen:NewsDetail
        // }
    },
    { 
        portraitOnlyMode: true
    }
)


export default TabMain = TabNavigator(
    {
        News:{
            screen:NewsTab
        },
        Home:{
            screen:Home
        },
        Customer:{
            screen:Customer
        }
    },{
        // initialRouteName: 'Customer',
        // tabBarPosition: 'bottom',
        swipeEnabled: false,
        animationEnabled: false,
        tabBarOptions: {
            activeTintColor: '#FFF',
            labelStyle: {
              fontSize: 10,
            },
            style: {
              backgroundColor: '#1976d2',
            },
            showIcon: true,
            showLabel: (Platform.OS === 'ios') ? true : false,
            upperCaseLabel: false,
            iconStyle: {
                width: 26,
                height: 26,
                // color:'white'
            }
        },
        lazy: true,
    }
)