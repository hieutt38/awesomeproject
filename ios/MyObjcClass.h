//
//  MyObjcClass.h
//  AwesomeProject
//
//  Created by hieutt on 11/13/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef MyObjcClass_h
#define MyObjcClass_h

#import <React/RCTBridgeModule.h>

@interface MyObjcClass : NSObject <RCTBridgeModule>

@end


#endif /* MyObjcClass_h */
