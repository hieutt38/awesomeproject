//
//  MyObjcClass.m
//  AwesomeProject
//
//  Created by hieutt on 11/13/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObjcClass.h"

@implementation MyObjcClass

// The React Native bridge needs to know our module
RCT_EXPORT_MODULE()

- (NSDictionary *)constantsToExport {
  return @{@"greeting": @"Welcome to the DevDactic\n React Native Tutorial!"};
}

RCT_EXPORT_METHOD(squareMe:(int)number:(RCTResponseSenderBlock)callback) {
  callback(@[[NSNull null], [NSNumber numberWithInteger: [self squareMe:number]]]);
}

- (NSInteger)squareMe:(int)number {
  return number*number;
}

@end
